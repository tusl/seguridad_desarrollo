# Programa curricular y planificación del aula virtual

Repositorio de la materia _Seguridad en el desarrollo de software_, de
la _Tecnicatura Universitaria en Software Libre_ de la Facultad de
Ingeniería y Ciencias Hídricas, Universidad Nacional del Litoral.

El _temario mínimo_ a cubrir por la materia son:

1. Desarrollo de aplicaciones seguras •
   [Lecturas](./lecturas/1_Desarrollo_de_aplicaciones_seguras),
   [Actividades](./actividades/1_Desarrollo_de_aplicaciones_seguras)
2. Diferentes tipos de ataques •
   [Lecturas](./lecturas/3_Tipos_de_ataque),
   [Actividades](./actividades/3_Tipos_de_ataque)
    - DoS
    - Inyección de código
    - etc.
3. Riesgos inherentes al entorno web, de software •
   [Lecturas](./lecturas/2_Riesgos_inherentes),
   [Actividades](./actividades/2_Riesgos_inherentes)
4. Buenas prácticas en seguridad •
   [Lecturas](./lecturas/4_Buenas_prácticas),
   [Actividades](./actividades/4_Buenas_prácticas)
    - Gestión de permisos y privilegios
    - Almacenamiento y manipulación de contraseñas
    - etc.
5. Normativas para el manejo de datos (habeas data) •
   [Lecturas](./lecturas/5_Normativas),
   [Actividades](./actividades/5_Normativas)
6. Licencias de software • [Lecturas](./lecturas/6_Licencias),
   [Actividades](./actividades/6_Licencias)
    - Consideraciones éticas y legales en el uso de bibliotecas y
      software de terceros, y en la publicación de software propio.

### ¡Ojo!

Por decisión del docente, este repositorio no sólo incluye las
lecturas y materiales empleados para impartirla, sino que guarda
registro de las _actividades_ realizadas, incluyendo las respuestas de
los alumnos, por el valor que éstas demostraron para el desarrollo
global del discurso y del material. Claro está, cada vez que este
curso se desarrolle, las actividades serán diferentes. Queda como
registro histórico.

## Descripción de la materia

Esta asignatura apunta a que los sustentantes sean capaces de llevar a
cabo sus desarrollos de software de forma responsable, evitando caer
en las vulnerabilidades más conocidas, y comprendiendo el origen de
dichos problemas.

La materia se desarrolla en el transcurso de diez semanas; los temas
están profundamente interrelacionados, por lo cual se busca cubrirlos
de forma trasversal, no se presentan separados en unidades.

## Cronograma de trabajo

El cursado de la materia inicia el 11.09.2017 y dura 10 semanas, hasta
el 13.11.2017.

## Materiales y bibliografía

El contenido de la materia será puesto a disposición en cada uno de los planes de trabajo a
medida que se vaya desarrollando la materia.

Toda lectura anexa, ya sea ésta libros o material disponible en la web, se irá indicando en
cada una de las unidades.

## Equipo docente

Gunnar Wolf es Licenciado en Ingeniería en Software por la Secretaría
de Educación Pública (acuerdo 286), Especialista en Seguridad
Informática y Tecnologías de la Información por la Escuela Superior de
Ingeniería Mecánica y Eléctrica (ESIME) del Instituto Politécnico
Nacional (IPN), todos ellos de México.

Es desarrollador del proyecto Debian desde el 2003.

## Licenciamiento

Todo el material desarrollado por el docente para esta materia se hace
disponible bajo un licenciamiento
[Creative Commons Atribución CompartirIgual 4.o](http://creativecommons.org/licenses/by-sa/4.0/deed.es).
