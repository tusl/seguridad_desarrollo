# Lecturas para el tema 1: Desarrollo de aplicaciones seguras

## Descripción de actividad:

Al iniciar la materia, se abre la discusión pidiendo que los
participantes acuerden algunos términos básicos en el foro:

- Seguridad
- Ataque
- Ética
- Hacker
- Defensa
- Integridad
- Disponibilidad
- Usabilidad
- Autenticación
- Autorización

Ver en la plataforma si es posible abrir foros para que esta
definición se vaya cerrando en grupos chicos a diario, o algo por el
estilo.

Hacia la mitad de la semana, darles la siguiente lista de
lecturas. Recomendar que cada quién lea dos o tres textos distintos
para ayudarlos en su definición.

## Lecturas

A continuación, les propongo una serie de lecturas. Muchas de ellas
exceden el ámbito de este primer módulo, y puede que sean
contradictorias entre sí. Son resultado de una búsqueda casi ciega en
línea: _Desarrollo de aplicaciones seguras_.

Elijan dos o tres lecturas al azar cada uno de ustedes, esperando
cubrirlos todos. 

- Sergio Talens-Oliag, 2004:
  [Seguridad en el desarrollo de aplicaciones](http://www.uv.es/~sto/charlas/SDA/SDA.pdf)
- Pablo Milano, 2007:
  [Seguridad en el ciclo de vida del desarrollo de software](http://www.cybsec.com/upload/cybsec_Tendencias2007_Seguridad_SDLC.pdf)
- Fernando Tricas García, 2008:
  [Desarrollo de aplicaciones seguras](https://es.slideshare.net/fernand0/desarrollo-de-aplicaciones-segurias)
- Juan Luis Serradilla Amarilla, 2010:
  [Normas para el Desarrollo de Aplicaciones Web Seguras](http://www.um.es/atica/documentos/NORowasp.pdf)
- Omar Alejandro Herrera Reyna, 2010:
  [Seguridad en el desarrollo de software](https://candadodigital.blogspot.mx/2010/11/seguridad-en-el-desarrollo-de-software.html?_escaped_fragment_#!)
- Denise Giusto Bilić, 2015:
  [10 consejos para el desarrollo seguro de aplicaciones](https://candadodigital.blogspot.mx/2010/11/seguridad-en-el-desarrollo-de-software.html?_escaped_fragment_#!)

## Lecturas para otro momento
- Antonio Domínguez Groba, 2011:
  [Desarrollo de aplicaciones seguras](http://www.hermessistemas.com/sites/hermessistemas.com/files/1%20Seguridad%20Telematica%202011%20-%20aplicaciones%20seguras%20Agroba.pdf)
