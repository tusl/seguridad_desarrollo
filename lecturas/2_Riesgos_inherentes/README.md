# Lecturas para el tema 2: Riesgos inherentes al entorno

- Web:
  - [OWASP ModSecurity Core Rule Set (CRS): The 1st Line of Defense Against Web Application Attacks](https://www.modsecurity.org/crs/)
  - [Dramatically Reducing Software Vulnerabilities](http://nvlpubs.nist.gov/nistpubs/ir/2016/NIST.IR.8151.pdf)
