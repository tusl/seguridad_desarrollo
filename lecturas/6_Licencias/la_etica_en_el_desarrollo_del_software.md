# La ética en el desarrollo del software

Como punto de partida para esta sección, les presento la traducción de
un blogazo reciente de Lars Wirzenius ([Ethics in software
development](https://yakking.branchable.com/posts/ethics/)). Lars,
además de ser uno de los más veteranos desarrolladores de Debian, es
quien (siendo aún estudiante de licenciatura) le explicó a Linus
Torvalds la filosofía de la licencia GPL, convenciéndolo de liberar el
desarrollo de juguete con que estaba iniciando.  

## La ética en el desarrollo de softare

El desarrollo del software libre siempre ha tenido una dimensión
ética. Desarrollamos software libre en vez de propietario para
permitir que la gente, los usuarios de nuestro software, no estén
tanto bajo el control de los vendedores de software propietario como
de otro modo estarían.

La libertad del software es, sin embargo, sólo una dimensión ética. El
software libre puede ser también no ético, por ejemplo, si hace cosas
que causan daño a la gente. Como un ejemplo extremo, una
implementación libre de ransomware sería muy problemática,
independientemente de la licencia que eligiera. No importa si el
programa está liberado bajo, digamos, la licencia GPL; si ataca las
computadoras de personas y cifra su información, y se niega a
descifrarla hasta que se le pague un rescate al autor del programa. Ni
siquiera si el program instala su propio código fuente en la
computadora en la que se cifra la información del usuario — No hay
manera en que esto pudiera considerarse ético.

Cuando escribes software, debes considerar la ética, la moralidad, el
impacto en todos. Por ejemplo:

- ¿Promueve racismo, sexismo o violencia, directa o indirectamente?
  Por ejemplo, si es "inteligencia artificial" que intenta determinar
  si alguien cometerá un crimen en el futuro, ¿su resultado se basa
  efectivamente sólo en su raza?
- ¿Utiliza ancho de banda innecesariamente? El ancho de banda es un
  artículo de lujo en algunas partes del mundo, así que desperdiciarlo
  discrimina contra la gente en dichas regiones.
- ¿"Llama a casa", así sea para reportar el uso a los desarrolladores?
  Esto violaría la privacidad del usuario. Compilar estadísticas de
  uso puede ser muy útil de diferentes maneras para los
  desarrolladores, pero hacerlo sin pedir permiso sigue siendo una
  violación de la privacidad.

¿Has encontrado software problemático éticamente? ¿O tal vez has
encontrado algún caso ejemplar de desarrollo ético de software? ¿Por
qué no dejar un ejemplo en los comentarios a continuación?

## De los comentarios

El breve blogazo de Lars termina ahí; es claramente un primer volcado de pensamientos, no busca ser un artículo académico completo ni nada por el estilo. Vale la pena detenerse en algunos de los comentarios.

## Otras lecturas

Lesley Mitchell comenta con varias lecturas relacionadas que pueden ser de gran interés:

- De la Wikipedia en inglés, [Programming
  Ethics](https://en.wikipedia.org/wiki/Programming_ethics). Menciona
  algo de historia y varios lineamientos que impulsan
  organizaciones profesional/académicas como la ACM:
  - Contribuir a la sociedad y al bienestar humano
  - Evitar dañar a otros
  - Ser honesto y confiable
  - Dar crédito adecuado a la propiedad intelectual
  - Respetar la privacidad de otros
  - Honrar la confidencialidad
  - Aprobar el software únicamente si tenemos una base bien fundamentada para creer que es seguro y cumple con las especificaciones
  - Aceptar plena responsabilidad por nuestro propio trabajo
  - No usar a sabiendas software que ha sido obtenido de forma ilegal o falta de ética
  - Identificar, definir y atacar puntos éticos, económicos, culturales, legales y ambientales al desarrollar proyectos laborales
  - Asegurar que las especificaciones para el software en el cual trabajamos satisfagan los requisitos del usuario y tengan las aprobaciones pertinentes
  - Asegurar pruebas, depuración y revisión adecuados para el software
  - No involucrarse en prácticas financieras engañosas como sobornos, contabilidad engañosa, u otras prácticas financieras impropias
  - Mejorar la capacidad de crear software de calidad, seguro, confiable y usable.
- De la [Enciclopedia Filosófica de Stanford](https://plato.stanford.edu/),
  - [Ética de computadoras y de la información](https://plato.stanford.edu/entries/ethics-computer/)
  - [La computación y la responsabilidad moral](https://plato.stanford.edu/entries/computing-responsibility/)
- De la Open University, hay un curso en línea gratuito llamado
  [Introducing ethics in information and computer
  sciences](http://www.open.edu/openlearn/education/educational-technology-and-practice/educational-technology/introducing-ethics-information-and-computer-sciences/content-section-0?active-tab=description-tab)
  (introducción a la ética en las ciencias de la computación y la
  información)

## ¿Puede valorarse éticamente una herramienta?

Otro comentario apunta a una pregunta concisa: ¿Puede una herramienta
valorarse éticamente? El comentario se refiere al primer punto del
artículo de Lars. Dice:

    Es una proposición resbalosa, dado que podríamos fácilmente poner
    a GPG/TOR/GNUNet (y, siguiendo holgadamente tu ejemplo, R), en la
    canasta de las cosas malas.

    Un profesional de la medicina cura a la gente independientemente
	de quién se trate. Un abogado defiende a su cliente, sea éste
	quien sea. ¿Esperas de una persona que contribuye al software
	libre un mejor equipamiento para comprender los aspectos de esta
	caja de pandora deontológica?

Entonces... Tenemos ya suficientes puntos como para generar
controversia. ¡Comenten lo que les llame la atención de este punto!
