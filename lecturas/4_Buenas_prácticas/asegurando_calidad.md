# Asegurando calidad en el software mediante la prevención de negligencia.

**Autora: Robin K. Hill.**

**Traducción: Yesica Pérez Navarro, @yesn7**

La importancia de la ética en torno a la tecnología goza de una
popularidad creciente, la cual es evidente en la preocupación que
existe en temas como la inteligencia artificial, las amenazas a la
privacidad, la brecha digital, la fiabilidad de los resultados de la
investigación y la vulnerabilidad del software. Esta preocupación por
el software se manifiesta en los esfuerzos de ciberseguridad y códigos
profesionales[<sup>1</sup>](#fn1). Los sombreros negros (black hats)
son hackers que utilizan software como arma con intención maliciosa, y
los sombreros blancos (white hats) son las organizaciones que
establecen medidas de seguridad contra productos defectuosos. Pero
tenemos un problema de sombreros grises- negligencia.

Mi impresión es que los criterios con los que solía evaluar los
programas de alumnos - pensamiento riguroso, diseño y pruebas,
limpieza de variables al inicializar, nombres de variables que den
información, cobertura completa de casos de prueba, modularización
cuidadosa - han sido abandonados o debilitados. Me ha sorprendido
descubrir, en prestigiosas instituciones que trabajan en proyectos de
código abierto, que los desarrolladores no producen documentación
alguna, como es natural, y que además, durante los ciclos de
mantenimiento, no corrigen los antiguos comentarios del código
fuente, considerando dichas ediciones arriesgadas y
presuntuosas. Todas estas personas son buenas programando, además es
gente buena. Sus prácticas parecen extrañamente razonables en esas
circunstancias, bajo la presión de la urgencia, aún cuando estas
prácticas degradan la comprensibilidad del programa. Si a esto le
sumamos la complejidad de los programas modernos, concluimos que, en
algunos casos, los programadores simplemente no saben lo que hace el
código.

Ejemplos de deficiencias en la calidad del software me vienen volando
a la mente- valores fuera de limites, condiciones complejas que
identifican los casos equivocados, inicializaciones a la constante
equivocada. Imagine a una programador inteligente y responsable,
terminando un módulo de calendario antes de una reunión
importante.Ella sabe que la prueba para los años bisiestos a partir de
un valor numérico yyyyy, if (yyyy mod 4 = 0) y (yyyy mod 100! = 0),
debe ser refinada por otras reglas para corregir lo que ocurre en
periodos más largos, pero este código es un prototipo... Ella continua
en la tarea, lo que implica buscar por información especifica, pero su
jefe le hace commit al código. No hace daño a nadie... excepto que
resulta que su módulo interactúa con otro módulo, donde el cálculo del
año bisiesto incorpora el conjunto completo de condiciones, y como
resultado se conduce la ejecución por el camino equivocado en algunos
cálculos. El programa está destinado a ser depurando pero sigue
corriendo, porque quienes conocen el error, lo compensan de alguna
manera...

Entonces, ¿qué tipo de violación es la negligencia? No ataca la
seguridad porque ocurre detrás del firewall. No ataca los criterios de
calidad porque nadie discute oficialmente esos criterios. Es una falta
de capacidad, una falta de prestar la debida atención y de tomarse las
molestias suficientes. ¿Puede la filosofía ayudar a aclarar lo que
está mal? Una teoría emergente llamada ética del cuidado desplaza la
moralidad clásica del deber y la justicia centrada en el agente,
respaldando en cambio la moralidad centrada en el paciente como se
manifiesta en las relaciones en tiempo
real<sup>[2](#fn2),[4](#fn4)</sup>. La teoría ofrece una perspectiva
contextual en lugar de las simples pautas de las visiones más
tradicionales. Mientras que el cuidado puede construirse como una
virtud (lo cual se relaciona con un texto previo mío en este mismo
espacio[<sup>3</sup>](#fn3)) o como un objetivo como la justicia, los
promotores de l ética del cuidado se resisten a un mandato
universal. Pueden también rechazar este intento de aplicarlo al
software, de entre todas las cosas; el corazón del asunto de la ética
del cuidado es llevar los cuidados a la persona que los necesite.

Sin embargo, la negligencia en el software parece exactamente el tipo
de transgresión que aborda la ética del cuidado, si permitimos su
reinterpretación fuera de las relaciones humanas. Apelar esta teoría
nos permite identificar lo contrario de cuidado, es decir, a la
negligencia como una cualidad a condenar.  Esto nos da cuenta de que
la calidad del software es un tema ético, especialmente acentuado en
su aplicación de herramientas que van desde la formación feminista
hasta la cultura que usa el código guerrero. Pero poco crédito es
merecido! No estamos resolviendo el problema, sino que lo incrustamos
en una plataforma filosófica. Esta relación plantea cuestiones en la
ética de la ingeniería, como la responsabilidad individual frente a la
responsabilidad corporativa (y si la responsabilidad corporativa puede
ser coherente y exigible por falta de legalidad). Para un resumen
conciso, véase la Sección 3.3.2, sobre Responsabilidad, en la
Enciclopedia de Filosofía de Stanford, en la entrada sobre la
Filosofía de la Tecnología[<sup>5</sup>](#fn5).

La cualidad que ha corregido a la negligencia en el pasado es el
profesionalismo, y con esto me refiero a que el experto hace lo que es
mejor para el cliente incluso a un costo de tiempo personal, energía,
dinero o prestigio, ¡y con toda razón! Ciertamente estos juicios son
subjetivos, y viables cuando el profesional es autónomo, cuando esa
persona única ejerce control sobre el producto y su calidad. Las
contrapartes en el mundo de los negocios tecnológicos actuales son (1)
el empleo, en el cual la mayoría de los programadores no son
consultores, sino que reciben órdenes de una empresa; y (2) la
colaboración, en donde la mayoría del softw la cualare es el producto
de los comités. El profesionalismo también depende de una fuerte
identificación personal con sus colegas y del orgullo en las
tradiciones del grupo.

De frente a las dificultades en sostener o promover los ideales de
calidad, una solución posible, por raro que suene, es simplemente
reconocer la situación, admitir al público que el software no siempre
es confiable, o maduro, o siquiera comprendido. Dada su familiaridad
con las correcciones de fallos, el público puede no sorprenderse
demasiado. Si preferimos rechazar esa jugada fatal, la pregunta
urgente es, ¿existen estándares públicos que los desarrolladores
puedan y vayan a seguir? La respuesta colectiva determinará si la
ingeniería de software es una profesión. Llamo a todos los
programadores que quieran enorgullecerse de sus trabajos leer los
borradores de los estándares profesionales[<sup>1</sup>](#fn1), que
mencionan a la calidad del código en su sección 2.1.

Vemos que los dilemas éticos aparecen no únicamente en el contexto
social externo, sino que también en el corazón del software, en la
práctica misma de la programación, un problema de sombrero gris si
quieres verlo así. Esperamos que las éticas del cuidado de algún modo
puedan ayudar a aliviar estos dilemas.
ferences

## Referencias

[<a name="fn1">1</a>] Association for Computing Machinery. Code 2018
    Project. [https://ethics.acm.org/](https://ethics.acm.org/)

[<a name="fn2">2</a>] Burton, B.K., and Dunn, C.P. Ethics of
    Care. Encyclopædia Britannica,
    [https://www.britannica.com/topic/ethics-of-care](https://www.britannica.com/topic/ethics-of-care)

[<a name="fn3">3</a>] Hill, R.K. Ethical Theories Spotted in Silicon
    Valley. Blog@CACM, March 16, 2017,
    [https://cacm.acm.org/blogs/blog-cacm/214615-ethical-theories-spotted-in-silicon-valley/fulltext](https://cacm.acm.org/blogs/blog-cacm/214615-ethical-theories-spotted-in-silicon-valley/fulltext)

[<a name="fn4">4</a>] Sander-Staudt, M. Care Ethics. The Internet
    Encyclopedia of
    Philosophy, 2017. [http://www.iep.utm.edu/care-eth/](http://www.iep.utm.edu/care-eth/)

[<a name="fn5">5</a>] Franssen, M., Lokhorst, G., and van de Poel,
    I. Philosophy of Technology. The Stanford Encyclopedia of
    Philosophy (Fall 2015 Edition), Edward N. Zalta
    (ed.). [https://plato.stanford.edu/archives/fall2015/entries/technology/](https://plato.stanford.edu/archives/fall2015/entries/technology/)
