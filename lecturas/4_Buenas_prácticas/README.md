# Lecturas para el tema 4: Buenas prácticas en seguridad

- [Undefined behavior in 2017](https://blog.regehr.org/archives/1520)
- [Assuring Software Quality By Preventing Neglect](https://cacm.acm.org/magazines/2017/9/220431-assuring-software-quality-by-preventing-neglect/fulltext);
  [traducción al español por Yesica Pérez Navarro](./asegurando_calidad.md)
