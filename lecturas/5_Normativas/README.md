# Normativas para el manejo de datos (habeas data)

Este no es un tema menor, y merece atención y elaboración. Sin
embargo, tras darle varias vueltas, me parece claro que yo no voy a
alcanzar a abordarlo correctamente; carezco por completo del contexto
necesario para el marco normativo argentino (incluso del
mexicano)... dejaré algunos apuntadores a reflexiones en este tema en
estos marcos jurídicos latinoamericanos y en el chileno (pues tengo un
artículo al respecto), y los invito a tratarloa manera de comparación
general.

Como primer apuntador, refiéranse a la página de [Derecho de acceso a
la información de
Wikipedia](https://es.wikipedia.org/wiki/Derecho_de_acceso_a_la_informaci%C3%B3n),
que tiene incisos para la situación por cada país.

- Argentina
  - Como recurso formal que me dieron los coordinadores de la UNL para
    preparar este tema: De la Biblioteca y Centro de Documentación del
    Ministerio de Justicia y Derechos Humanos, [Habeas data,
    legislación, biblioteca digital del Ministerio de Justicia,
    Seguridad y Derechos
    Humanos](http://www.biblioteca.jus.gov.ar/habeas-data.html)

- Chile
  - Texto de Derechos Digitales: [¿Qué dice el llamado Decreto
    Espía?](https://www.derechosdigitales.org/11400/que-dice-el-llamado-decreto-espia/)

- México
  - Tenemos varios marcos jurídicos que se acercan al tema;
    particularmente resultan relevantes, en dos vertientes muy
    distintas: El acceso que se debe brindar a todos usuario a la
    información pública y el uso de información personal a que está
    obligado todo quien la recaba para cualquier fin
    1. [Ley General de Transparencia y Acceso a la Información
       Pública](http://www.diputados.gob.mx/LeyesBiblio/pdf/LGTAIP.pdf)
    2. [Ley Federal de Protección de Datos Personales en Posesión de
        Particulares](http://www.diputados.gob.mx/LeyesBiblio/pdf/LFPDPPP.pdf)

  - Vale la pena referir a la Wikipedia, que presenta la
    [LGTAIP](https://es.wikipedia.org/wiki/Ley_Federal_de_Transparencia_y_Acceso_a_la_Informaci%C3%B3n_P%C3%BAblica_Gubernamental)
    y la
    [LFPDPPP](https://es.wikipedia.org/wiki/Ley_Federal_de_Protecci%C3%B3n_de_Datos_Personales)
    más "digerida" y comprensible.
  - Respecto a la LGTAIP, les dejo la liga a la conferencia
    ([video](https://www.youtube.com/watch?v=1ajic6FEtnI),
    [presentación](https://es.slideshare.net/VctorMartinez/los-portales-de-transparencia-y-los-datos-abiertos-en-las-universidades-pblicas-80993323))
    que presentó el 18 de octubre Víctor Manuel Martínez en el
    Encuentro en Línea de Educación, Cultura y Software Libre,
    ilustrando cómo frecuentemente se cumple con la letra de la ley,
    pero no con su espíritu (ilustrando la dificultad de dar uso a los
    datos compartidos requeridos por la ley)

